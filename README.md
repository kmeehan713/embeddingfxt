# EmbeddingFXT

This is code to be used on PDSF to run over the minimc files and produce embedding QA.
It also produces the histograms used to make the TPC acceptance + efficiency
and energy loss correction curves. The output can be copied to menkar/nuclear and 
passed as input to RunMakeEmbeddingCorrectionCurves.C